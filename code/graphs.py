from .pendulum import *
import matplotlib.pyplot as plt
import numpy as np

DEBUG = False

# Implements the harmonograph shown here: http://andygiger.com/science/harmonograph/index.html
class TwoSpinning(object):
    def __init__(self, graphA, graphB, graphC, Ra=400, Rb=400, dist=10, h=10):
        if not isinstance(graphA, Pendulum):
            raise ValueError('graphA is not a pendulum')
        if not isinstance(graphB, Pendulum):
            raise ValueError('graphB is not a pendulum')
        if not isinstance(graphC, Pendulum):
            raise ValueError('graphC is not a pendulum')

        self.graphA = graphA
        self.graphB = graphB
        self.graphC = graphC

        self.Ra = Ra
        self.Rb = Rb
        self.dist = dist
        self.h = h

    def calc(self, time):
        Ax, Ay = -self.dist / 2, self.h
        Bx, By = self.dist / 2, self.h

        grA_x = self.graphA.calc_x(time) + Ax
        grA_y = self.graphA.calc_y(time) + Ay
        grB_x = self.graphB.calc_x(time) + Bx
        grB_y = self.graphB.calc_y(time) + By
        grC_x = self.graphC.calc_x(time)
        grC_y = self.graphC.calc_y(time)

        dx = grB_x - grA_x
        dy = grB_y - grB_x

        AB_dist = np.sqrt(dx ** 2 + dy ** 2)

        a = ((self.Ra * self.Ra - self.Rb * self.Rb + AB_dist * AB_dist) / (2 * AB_dist))
        h = np.sqrt(self.Ra * self.Ra - a * a)

        ab_x = grB_x + h * (grB_y - grA_y) / AB_dist
        ab_y = grB_y - h * (grB_x - grA_x) / AB_dist

        if DEBUG:
            fig = plt.figure()
            ax = fig.gca()
            fig.suptitle('Pendulums')
            ax.plot(grA_x, grA_y, label='Graph A')
            ax.plot(grB_x, grB_y, label='Graph B')
            ax.plot(grC_x, grC_y, label='Graph C')
            ax.plot(ab_x, ab_y, label='Graph A+B')
            ax.legend()
            plt.draw()

            fig2 = plt.figure()
            ax2 = fig2.gca()
            fig2.suptitle('A B Distance')
            ax2.plot(time, AB_dist)
            plt.draw()

        final_x = ab_x - grC_x
        final_y = ab_y - grC_y

        return final_x, final_y

class TwoRotating(object):
    def __init__(self, graphA, graphB, graphC, Ra=400, Rb=400, dist=10, h=10):
        if not isinstance(graphA, Pendulum):
            raise ValueError('graphA is not a pendulum')
        if not isinstance(graphB, Pendulum):
            raise ValueError('graphB is not a pendulum')
        if not isinstance(graphC, RotatingPendulum):
            raise ValueError('graphC is not a rotating pendulum')

        self.graphA = graphA
        self.graphB = graphB
        self.graphC = graphC

        self.Ra = Ra
        self.Rb = Rb
        self.dist = dist
        self.h = h

    def calc(self, time):
        Ax, Ay = -self.dist / 2, self.h
        Bx, By = self.dist / 2, self.h

        grA_x = self.graphA.calc_x(time) + Ax
        grA_y = self.graphA.calc_y(time) + Ay
        grB_x = self.graphB.calc_x(time) + Bx
        grB_y = self.graphB.calc_y(time) + By

        dx = grB_x - grA_x
        dy = grB_y - grB_x

        AB_dist = np.sqrt(dx ** 2 + dy ** 2)

        a = ((self.Ra * self.Ra - self.Rb * self.Rb + AB_dist * AB_dist) / (2 * AB_dist))
        h = np.sqrt(self.Ra * self.Ra - a * a)
        ab_x = grB_x + h * (grB_y - grA_y) / AB_dist
        ab_y = grB_y - h * (grB_x - grA_x) / AB_dist

        if DEBUG:
            fig = plt.figure()
            ax = fig.gca()
            fig.suptitle('Pendulums')
            ax.plot(grA_x, grA_y, label='Graph A')
            ax.plot(grB_x, grB_y, label='Graph B')
            ax.plot(ab_x, ab_y, label='Graph A+B')
            ax.legend()
            plt.draw()

            fig2 = plt.figure()
            ax2 = fig2.gca()
            fig2.suptitle('A B Distance')
            ax2.plot(time, AB_dist)
            plt.draw()

        final_p = self.graphC.calc(time, ab_x, ab_y)

        xv = [ final_p[i][0] for i in range(0, final_p.shape[0]) ]
        yv = [ final_p[i][1] for i in range(0, final_p.shape[0]) ]

        return xv, yv, ab_x, ab_y

