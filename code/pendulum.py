import numpy as np


class Pendulum(object):
    def __init__(self, A_x, A_y, f, d, p_x=0, p_y=0):
        # A: Amplitude, p: phase d: damping, f: frequency
        # https://en.wikipedia.org/wiki/Harmonograph
        self.A_x = A_x # px
        self.A_y = A_y # px
        self.f = f     # Hz
        self.d = d     # s
        self.p_x = p_x # rad
        self.p_y = p_y # rad

    def calc_x(self,t):
        return self.A_x*np.exp(-1*t/self.d)*np.sin(2*np.pi*self.f*t+self.p_x)

    def calc_y(self,t):
        return self.A_y*np.exp(-1*t/self.d)*np.sin(2*np.pi*self.f*t+self.p_y)


class RotatingPendulum(object):
    def __init__(self, A, f, d, l):
        self.A = A
        self.d = d
        self.f = f
        self.l = l

    def calc_normal(self, t):
        t_exp = np.exp(-1*t/self.d)
        x = self.A*t_exp*np.cos(t*self.f)
        y = self.A*t_exp*np.sin(t*self.f)

        z = np.sqrt(self.l*self.l - np.power(t_exp, 2))

        return np.array([
            x/self.l,
            y/self.l,
            z/self.l
        ])

    def calc_primev(self, t, x, y, norms):
        Ae = self.A*np.exp(-1 * t / self.d)
        cos_theta = np.sqrt(1-np.power(Ae/self.l, 2))

        z0 = (self.l*(1-cos_theta)-norms[0]*x-norms[1]*y)/norms[2]

        xp = x-(Ae*np.cos(t*self.f))
        yp = y-(Ae*np.sin(t*self.f))
        zp = z0+self.l*(1-cos_theta)

        return np.array([xp, yp, zp])

    def calc_trigs(self, norms):
        sin_1 = (norms[1]/norms[2])/np.sqrt(1+np.power(norms[1]/norms[2], 2))
        sin_2 = norms[0]

        cos_1 = np.sqrt(1-np.power(sin_1, 2))
        cos_2 = np.sqrt(1-np.power(sin_2, 2))

        return np.array([
            sin_1,
            cos_1,
            sin_2,
            cos_2
        ])

    def calc_points(self, p_points, trigs):
        size = trigs[0].shape[0]

        mat1 = np.array([
            [
                [trigs[3][i], 0, -1*trigs[2][i]],
                [0, 1, 0],
                [trigs[2][i], 0, trigs[3][i]]
            ]
            for i in range(0, size)
        ])

        mat2 = np.array([
            [
                [1, 0, 0],
                [0, trigs[1][i], trigs[0][i]],
                [0, -1*trigs[0][i], trigs[1][i]]
            ]
            for i in range(0, size)
        ])

        """
        mat1 = np.array([
            [trigs[3], np.zeros(trigs[0].shape[0]), -1*trigs[2]],
            [np.zeros(trigs[0].shape[0]), np.ones(trigs[0].shape[0]), np.zeros(trigs[0].shape[0])],
            [trigs[2], np.zeros(trigs[0].shape[0]), trigs[3]]
        ])

        mat2 = np.array([
            [np.ones(trigs[0].shape[0]), np.zeros(trigs[0].shape[0]), np.zeros(trigs[0].shape[0])],
            [np.zeros(trigs[0].shape[0]), trigs[1], trigs[0]],
            [np.zeros(trigs[0].shape[0]), -1*trigs[0], trigs[1]]
        ])
        """

        pp = np.array([
            [p_points[0][i], p_points[1][i], p_points[2][i]]
            for i in range(0, size)
        ])

        each_1 = np.array([
            pp[i].dot(mat2[i])
            for i in range(0, size)
        ])

        each_2 = np.array([
            each_1[i].dot(mat1[i])
            for i in range(0, size)
        ])

        print(each_2)

        return each_2

    def calc(self, t, x, y):
        norms = self.calc_normal(t)
        p_points = self.calc_primev(t, x, y, norms)
        trigs = self.calc_trigs(norms)

        return self.calc_points(p_points, trigs)
