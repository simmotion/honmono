from code import *
from code.pendulum import *
import matplotlib.pyplot as plt
import numpy as np

freq = 100
t_max = 100

if __name__ == '__main__':
    """
    honmo = graphs.TwoSpinning(
        Pendulum(10, 10, 1, 18, 0, np.pi / 2),
        Pendulum(10, 10, 0.98, 18, np.pi / 2, 0),
        Pendulum(10, -10, 1, 8, 0, 0),
        Ra=20,
        Rb=20
    )
    t = np.arange(0, 0.005, 0.001)
    px, py = honmo.calc(t)

    fig = plt.figure()
    ax = fig.gca()
    ax.plot(px, py, label='Two Spinning')
    plt.show()
    """
    honmo = graphs.TwoRotating(
        #A, f, d, l
        Pendulum(10, 10, 1, 18, 0, np.pi / 2),
        Pendulum(10, 10, 0.98, 18, np.pi / 2, 0),
        RotatingPendulum(5, 10, 1000, 10),
        Ra=20,
        Rb=20
    )
    t = np.arange(0, t_max, 1/freq)
    px, py, abx, aby = honmo.calc(t)

    for t in range(1, t_max):
        fig = plt.figure()
        ax = fig.gca()
        fig.suptitle('Two Rotating - t: '+str(t-1)+'~'+str(t))
        ax.plot(px[(t-1)*freq:t*freq], py[(t-1)*freq:t*freq])
        fig.savefig('img/'+str(t)+'.png')

    for t in range(1, t_max):
        fig = plt.figure()
        ax = fig.gca()
        fig.suptitle('Two Rotating (AB) - t: '+str(t-1)+'~'+str(t))
        ax.plot(abx[(t-1)*freq:t*freq], aby[(t-1)*freq:t*freq])
        fig.savefig('img/ab_'+str(t)+'.png')
